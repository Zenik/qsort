#include <iostream>
#include <vector>

using namespace std;



// Input: A - contains input data to be sorted
//        l - leftmost index
//        r - rightmost index
int quickSort(auto& A, int l, int r )
{
  int count = 0;
  
  //... 
  
  return count;
}


int main()
{
  vector<int> inputs;
  int input;

   cerr<<"Welcome to \"quickSort Analysis\". We first need some input data."<<endl;
   cerr<<"To end input type Ctrl+D (followed by Enter)"<<endl<<endl;

   
 
    while(cin>>input)//read an unknown number of inputs from keyboard
    {
       inputs.push_back(input);
    }

   cerr<<endl<<"|  Number of inputs | Number of comparisons |"<<endl;
   cerr<<"|\t"<<inputs.size();
   cout<<"| "<<quickSort(inputs, 0, inputs.size() -1);
    
   cerr<<"\t|"<<endl<<endl<<"Program finished."<<endl<<endl;

    return 0;
}
